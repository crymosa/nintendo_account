package com.brown.nintendo.domain.account.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AccountServiceTest {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    @DisplayName("패스워드_암복호화_확")
    public void 패스워드암호화복호화(){
        //given
        final String password = "abcde1234";
        final String encodedPassword = passwordEncoder.encode(password);
        assertEquals(true, passwordEncoder.matches(password, encodedPassword));
    }
}