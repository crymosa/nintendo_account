package com.brown.nintendo.controller;

import com.brown.nintendo.domain.account.entity.Account;
import com.brown.nintendo.domain.account.AccountLoginDto;
import com.brown.nintendo.domain.account.AccountRequestDto;
import com.brown.nintendo.domain.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@ResponseBody
@Slf4j
@RequestMapping("/api/account")
@RequiredArgsConstructor
public class AccountController {
    final AccountService accountService;

    @GetMapping("/users")
    public ResponseEntity<?> getUsers(){
        return ResponseEntity.ok().body("accountList");
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody AccountRequestDto dto){
        accountService.createAccount(dto);
        return ResponseEntity.ok().body("create Uesr!");
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody AccountLoginDto loginDto){
        return ResponseEntity.ok().body(accountService.getLogin(loginDto));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> readUser(@PathVariable long id){
        Account account = accountService.getAccount(id);
        return ResponseEntity.ok().body(account);
    }
}
