package com.brown.nintendo.domain.account;

import com.brown.nintendo.domain.account.entity.Account;
import com.brown.nintendo.domain.account.enumtype.AccountState;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;


@Getter
public class AccountResponseDto {
    private Long accountKey;
    private String nickName;
    private String email;
    private String nintendoAccountId;
    private AccountState accountState;
    @JsonIgnore
    private String password;

    @Builder
    public AccountResponseDto(Long accountKey, String nickName, String email, String nintendoAccountId, AccountState accountState, String password) {
        this.accountKey = accountKey;
        this.nickName = nickName;
        this.email = email;
        this.nintendoAccountId = nintendoAccountId;
        this.accountState = accountState;
        this.password = password;
    }

    public AccountResponseDto(Account account){
        this.accountKey = account.getAccountKey();
        this.nickName = account.getNickName();
        this.email = account.getEmail();
        this.nintendoAccountId = account.getNintendoAccountId();
        this.accountState = account.getAccountState();
        this.password = account.getPassword();
    }
}
