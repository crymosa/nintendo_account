package com.brown.nintendo.domain.account.enumtype;

import com.brown.nintendo.common.BaseEnumCode;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum AccountState implements BaseEnumCode<String> {
    READY("0", "대기"),
    JOIN("1","승인");

    private String code;
    private String description;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
