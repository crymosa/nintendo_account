package com.brown.nintendo.domain.account.spec;

import com.brown.nintendo.domain.account.entity.Account;
import org.springframework.data.jpa.domain.Specification;

public class AccountSpec {
    public static Specification<Account> withEmail(String email){
        return (Specification<Account>) ((root, query, builder)->
            builder.equal(root.get("email"), email)
        );
    }
}
