package com.brown.nintendo.domain.account;

import com.brown.nintendo.domain.account.entity.Account;
import com.brown.nintendo.domain.account.enumtype.AccountState;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountRequestDto {
    private String nickName;
    private String email;
    private String nintendoAccountId;
    private String password;

    public Account toEntity(){
        return Account.builder()
                .email(this.email)
                .nickName(this.nickName)
                .nintendoAccountId(this.nintendoAccountId)
                .accountState(AccountState.READY)
                .password(this.password)
                .build();
    }
}
