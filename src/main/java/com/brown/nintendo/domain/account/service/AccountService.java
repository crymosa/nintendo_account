package com.brown.nintendo.domain.account.service;

import com.brown.nintendo.common.CommonMessage;
import com.brown.nintendo.domain.account.*;
import com.brown.nintendo.domain.account.entity.Account;
import com.brown.nintendo.domain.account.spec.AccountSpec;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class AccountService {
    final PasswordEncoder passwordEncoder;
    final AccountRepository accountRepository;

    public void createAccount(AccountRequestDto accountRequestDto){
        accountRequestDto.setPassword(passwordEncoder.encode(accountRequestDto.getPassword()));
        accountRepository.save(accountRequestDto.toEntity());
    }

    public Account getAccount(long id){
        return accountRepository.findById(id).orElseThrow(()->new EntityNotFoundException("아이디를 확인해주세요"));
    }

    public AccountResponseDto getLogin(AccountLoginDto accountLoginDto){
       Account account = accountRepository.findOne(AccountSpec.withEmail(accountLoginDto.getEmail()))
               .orElseThrow(()->new NoSuchElementException(CommonMessage.NO_VAILD_IDPASSWORD));

       if (!passwordEncoder.matches(accountLoginDto.getPassword(), account.getPassword())){
           throw new NoSuchElementException(CommonMessage.NO_VAILD_IDPASSWORD);
       } else {
           return new AccountResponseDto(account);
       }

    }
}
