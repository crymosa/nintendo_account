package com.brown.nintendo.domain.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountLoginDto {
    private String email;
    private String password;
}
