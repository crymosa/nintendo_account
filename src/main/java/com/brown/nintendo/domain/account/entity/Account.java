package com.brown.nintendo.domain.account.entity;

import com.brown.nintendo.common.converter.AccountStateConverter;
import com.brown.nintendo.domain.account.enumtype.AccountState;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
public class Account {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACCOUNT_KEY")
    private Long accountKey;

    @Column(name = "NICK_NAME",length = 50, nullable = false)
    private String nickName;

    @Column(name = "EMAIL", length = 70, nullable = false, unique = true)
    private String email;

    @Column(name = "NINTENDO_ACCOUNT_ID", length = 50, nullable = false)
    private String nintendoAccountId;

    @Column(name = "ACCOUNT_STATE", length = 10, nullable = false)
    @Convert(converter = AccountStateConverter.class)
    private AccountState accountState;

    @JsonIgnore
    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Builder
    public Account(String nickName, String email, String nintendoAccountId, AccountState accountState, String password) {
        this.nickName = nickName;
        this.email = email;
        this.nintendoAccountId = nintendoAccountId;
        this.accountState = accountState;
        this.password = password;
    }
}
