package com.brown.nintendo.config;

import com.brown.nintendo.common.model.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.PersistenceException;
import java.sql.SQLException;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class ControllerAdvice {
    final static String ERROR_CODE = "9999";

    @ExceptionHandler(SQLException.class)
    public ResponseEntity<?> customResponse(Exception e){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ResponseData.builder().code(ERROR_CODE).data(e.getMessage()).build());
    }

    @ExceptionHandler({PersistenceException.class, NoSuchElementException.class})
    public ResponseEntity<?> notFound(Exception e){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ResponseData.builder().code(ERROR_CODE).data(e.getMessage()).build());
    }
}
