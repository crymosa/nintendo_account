package com.brown.nintendo.common.model;

import lombok.Builder;
import lombok.Getter;

@Getter
public class ResponseData {
    private String code;
    private Object data;

    @Builder
    public ResponseData(String code, Object data) {
        this.code = code;
        this.data = data;
    }
}
