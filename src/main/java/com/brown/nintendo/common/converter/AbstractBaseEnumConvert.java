package com.brown.nintendo.common.converter;

import com.brown.nintendo.common.BaseEnumCode;

import javax.persistence.AttributeConverter;
import java.util.Arrays;

public abstract class AbstractBaseEnumConvert<X extends Enum<X> & BaseEnumCode<Y>, Y> implements AttributeConverter<X, Y> {
    protected abstract X[] getValueList();

    @Override
    public Y convertToDatabaseColumn(X attribute) {
        return attribute.getCode();
    }

    @Override
    public X convertToEntityAttribute(Y dbData) {
        return Arrays.stream(getValueList())
                .filter(e -> e.getCode().equals(dbData))
                .findFirst()
                .orElseThrow(()-> new IllegalArgumentException(String.format("Unsupported type for %s.", dbData)));
    }
}
