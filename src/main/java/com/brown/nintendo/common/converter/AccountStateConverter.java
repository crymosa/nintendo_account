package com.brown.nintendo.common.converter;

import com.brown.nintendo.domain.account.enumtype.AccountState;

import javax.persistence.Converter;

@Converter(autoApply = true)
public class AccountStateConverter extends AbstractBaseEnumConvert<AccountState, String>{

    @Override
    protected AccountState[] getValueList() {
        return AccountState.values();
    }
}
