package com.brown.nintendo.common;

public interface BaseEnumCode<T> {
    T getCode();
    T getDescription();
}
